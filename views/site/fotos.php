<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    
  //utilizando el helper de img  
?>

<?= Html::img("@web/imgs/{$foto}",['id'=>'foto','alt'=>'foto chula'])?>

<?php
    //utilizamos el helper URl
?>
<img src="<?= Url::to("@web/imgs/{$foto}")?>">

<?php // Utilizando el metodo alias?>
<img src="<?= Yii::getAlias('@web') . "/imgs/{$foto}"?>">
